package main

import("net/http")

type User struct {

	Name      string `json:"name"`
	Pass      string `json:"pass"`
	Birthdate string `json:"birthdate"`
	Gender    string `json:"gender"` 
} 

func RegisterUser(response http.ResponseWriter, request *http.Request) {

	body := request.Body

	user := User{}

	DecodeJson(body, &user)

	err := SaveUser(user)

	if err != nil{
		response.Write([]byte("400"))
		return
	}

	response.Write([]byte("200"))

}

func SaveUser(user User) (err error) {

	conn, err := GetConnection()

	collection := conn.DB("reprova").C("users")

	collection.Insert(user)

	return
}

func SearchUser(response http.ResponseWriter, request *http.Request) {
	
	req := request
	q := req.URL.Query()
	name := q.Get("name")
	gender := q.Get("gender")

	result, err := GetUser(name,gender)

	if err != nil{
		response.Write([]byte("400"))
		return
	}

	response.Header().Set("Content-Type", "application/json; charset=utf-8")
	j, _ := json.Marshal(result)
	response.Write(j)

}
